package Main;


import java.util.Scanner;

import CodFisc.CodFisc;
import MathFunctions.ArrayBid;
import MathFunctions.ArrayNum;
import MathFunctions.Calcolatrice;
import MathFunctions.CalcoloSomma;
import MathFunctions.ParieDispari;
import ModelClassExample.ModelTest;
import Threads.ExampleThread;
import user.Phone;
import user.User;


public class Main {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		startMenu();

		}
	
	
	public static void startMenu() {
		
	    System.out.println("\n**** BENVENUTO NELLA MIA APP ****");
	    System.out.println("\n**** MENU ****");

		System.out.println("Scegli un'opzione:\n");
		
		System.out.println("0, chiudi App");
		System.out.println("1, per calcolare la somma.");
		System.out.println("2, per la calcolatrice di interi.");
		System.out.println("3, per riempire un array numerico.");
		System.out.println("4, per riempire un array bidimensionale di nomi e cognomi.");
		System.out.println("5, per determinare se un numero e' pari o dispari.");
		System.out.println("6, per codice fiscale inverso."); //se nel menu del cod fisc metto una lettera per l'opzione, l'app si ferma
		System.out.println("7, per test classi model.");
		System.out.println("8, per test Thread.");

		Scanner inputA = new Scanner(System.in);
		
		itemMenu(inputA.nextInt());

	}
	

	
	public static void itemMenu(int opz) {
				
		switch(opz) {
		case 0: 
			System.out.println("Arrivederci!");
			System.exit(0); 
			break;
		case 1: 
			CalcoloSomma myCalcoloSomma = new CalcoloSomma();
			myCalcoloSomma.Start();
			break;
		case 2:
			Calcolatrice myCalcolatrice = new Calcolatrice();
			myCalcolatrice.Start();
			break;
		case 3: 
			ArrayNum myArrayNum = new ArrayNum();
		    myArrayNum.Start();
		    break;
		case 4:
			ArrayBid myArrayBid = new ArrayBid(); // non riesco a mettere l'opzione che rimanda al menu
			myArrayBid.Start();
			break;
		case 5: 
			ParieDispari myParieDispari = new ParieDispari();
			myParieDispari.Start();
			break;
		case 6:
			CodFisc myCodFisc = new CodFisc();
			myCodFisc.Start();
			break;
		case 7:
			ModelTest myModeltest = new ModelTest();
			myModeltest.Start();
			break;
		case 8:
			ExampleThread myExampleThread = new ExampleThread();
			myExampleThread.Start();
			break;
		default: 
			System.out.println("Opzione non valida, riprova.");  //voglio che a questo punto ritorni al menu principale
			startMenu();
	        
		}
	}
	
	
	
}