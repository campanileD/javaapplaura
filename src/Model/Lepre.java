package Model;

public class Lepre extends Animale{
	
	   @Override
	   public void movimento(Boolean isMovimento) {
	  		this.siMuove = isMovimento;
	  		
	  		if(isMovimento) {
	  			super.velocitaPasso += 2;
	  		} else {
	  			 this.velocitaPasso = 0;
	  		}
	  		
	  	}
	  	
	  	public int getVelocitaPasso() {		
	  		return super.velocitaPasso;
	  	}
}
