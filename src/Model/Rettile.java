package Model;

public class Rettile extends Animale {
	
	private String gruppo; // tartaruga, serpente, lucertola, coccodrillo...
	
	private Boolean deponeUovo = false;
	private int uovo = 0;
	
    public Rettile() {}
    
    public Rettile(String gruppo) {
    
    	this.gruppo = gruppo;
    }
    

    public String getGruppo() {
		return this.gruppo;
	}
    

    public void setGruppo(String gruppo) {
    	this.gruppo = gruppo;
    }
    
    

    
    //metodi personali
    public void faUovo(Boolean isUovo) {
		this.deponeUovo = isUovo;
		
		if(isUovo) {
			 this.uovo += 1;
			 
		     System.out.println(uovo);
		} else {
			 this.uovo = 0;
      }
    }
    
    public int getNumeroUova() {		
       return this.uovo;
}
}
