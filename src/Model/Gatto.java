package Model;

public class Gatto extends Animale {

		private String razza;
		private String colore;
	    private int eta;
	    
	    public Gatto() {}
	    
	    public Gatto(String razza, String colore, String genere, int eta) {
	    	
	    	this.razza = razza;
	    	this.colore = colore;
	    	this.eta = eta;
	    	
	    }
	    
	    
	    

	    public String getRazza() {
			return this.razza;
		}
	    

	    public void setRazza(String razza) {
	    	this.razza = razza;
	    }
	    
	    
	    public String getColore() {
			return this.colore;
		}
	    

	    public void setColore(String colore) {
	    	this.colore = colore;
	    }
	    
	 
	    
       // metodi personali
	    
		public void getEtaUmana() {
			int c = eta*5;
			System.out.println("Eta' in anni umani: " +  c);
			}
		
		public void Miagola() {
			System.out.println("Miao miao!");
		}
		
		public void Riproduci() {
			Gattino x = new Gattino();
			
		}
}

