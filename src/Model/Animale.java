package Model;

public class Animale {

 private String nome;
 private String classe; //mammifero, rettile, uccello, anfibio, insetto
 private String tipo;
 private String genere;
 private int eta;
 
 //attributi di azione
 
//Attributi di azione
 
 protected Boolean siMuove = false;
 protected int velocitaPasso = 0;
 
 private Boolean staMangiando = false;
 private int sazio = 0;
 private Boolean staBevendo = false;
 private int idratato = 0;

public Animale () {} //metodo senza parametri, costruttore di default che viene creato in automatico

public Animale(String nome, String classe, String tipo, String genere, int eta) {
	
	this.nome = nome;
	this.classe = classe;
	this.tipo = tipo;
	this.genere = genere;
	this.eta = eta;
}
	
//Metodi GET e SET


    public String getNome() {
	    return this.nome;
}
    
    public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return this.tipo;
		
	}
	
	public void setEta(int eta) {
		this.eta = eta;
	}
	
	public int getEta() {
		return this.eta;
	}
	
	public void setGenere(String genere) {
		this.genere = genere;
	}
	
	public String getGenere() {
		return this.genere;
	}
	
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	public String getClasse() {
		return this.classe;
	}
	
    public int getSazio() {		
		return this.sazio;
}
    public int getIdratato() {		
  		return this.idratato;
    }
	
//Metodi personali
	public void StampaTipo() {
		System.out.println(tipo);
	}
	
	
	
	public void mangia(Boolean isMangia) {
		this.staMangiando = isMangia;
		
      for (int i=1; i<11; i++) {
		if(isMangia) {
			 this.sazio += 1;
			 
		     System.out.println(i);
		} else {
			 this.sazio = 0;
      }
      }
	}
      public void beve(Boolean isBeve) {
  		this.staBevendo = isBeve;
  		
        for (int i=1; i<11; i++) {
  		if(isBeve) {
  			 this.idratato += 1;
  		} else {
  			 this.idratato = 0;
  		}
        }
        
    	}
      
      public void movimento(Boolean isMovimento) {
  		this.siMuove = isMovimento;
  		
  		if(isMovimento) {
  			 this.velocitaPasso += 1;
  		} else {
  			 this.velocitaPasso = 0;
  		}
  		
  	}
  	
  	public int getVelocitaPasso() {		
  		return this.velocitaPasso;
  	}

}

