package Model;

import java.util.Scanner;

public class Serpente extends Rettile {
	
	private String colore;
	private float metri;
	
	//Attributi di azione
		private Boolean staStrisciando = false;
		private int velocitaStriscia = 0;
		
		private Boolean faMuta = false;
	
	public Serpente () {}

	
	// costruttore di serpente
	public Serpente (String colore, float metri) {
		
		this.colore = colore;
		this.metri = metri;
	}
	
	// metodi getter e setter
	 public String getColore() {
			return this.colore;
		}
	    

	    public void setColore(String colore) {
	    	this.colore = colore;
	    }
	    
	    public float getMetri() {
			return this.metri;
		}
	    

	    public void setMetri(float metri) {
	    	this.metri = metri;
	    }
	    
	    public int getVelocitaStriscia() {
			return this.velocitaStriscia;
		}
	    

	    public void setVelocitaStriscia(int velocitaStriscia) {
	    	this.velocitaStriscia = velocitaStriscia;
	    }
	    
	    // metodi personali
	    public void striscia(Boolean isStriscia) {
			this.staStrisciando = isStriscia;
			
			if(isStriscia) {
				
			for (int i=0; i<velocitaStriscia; i++) {
				
				System.out.println("-");
			}
				
			} else {
				 this.velocitaStriscia = 0;
			}
			
			}

		public void muta(Boolean isMutante) {
			this.faMuta = isMutante;
			
			if(isMutante) {
				System.out.println("Scegli un colore nuovo:");
				Scanner color = new Scanner(System.in);
				colore = color.nextLine();
				
				this.setColore(colore);
				System.out.println(this.getColore());
			}
			
			else {
				this.colore = colore;
			}
		}
}
