package CodFisc;

import java.util.Scanner;

import Main.Main;

public class CalcoloMese {
	
	
	public void Calcolo (String codFisc) {
		
		
		
		String mese = " ";
		
//	      resituisce il mese di nascita e controlla che nel codice ci siano lettere valide
        
		    if (codFisc.charAt(8)== 'a' || codFisc.charAt(8)== 'A') { 
		    	mese +="gennaio"+ " ";
		    }
		    else if (codFisc.charAt(8)== 'b' || codFisc.charAt(8)== 'B') {
		    	mese +="febbraio" + " ";
		    }
		    else if (codFisc.charAt(8)== 'c' || codFisc.charAt(8)== 'C') {
		    	mese +="marzo" + " ";
		    }
		    else if (codFisc.charAt(8)== 'd' || codFisc.charAt(8)== 'D') {
		    	mese +="aprile" + " ";
		    }
		    else if (codFisc.charAt(8)== 'e' || codFisc.charAt(8)== 'E') {
		    	mese +="maggio"+ " ";
		    }
		    else if (codFisc.charAt(8)== 'h' || codFisc.charAt(8)== 'H') {
		    	mese +="giugno"+ " ";
		    }
		    else if (codFisc.charAt(8)== 'l' || codFisc.charAt(8)== 'L') {
		    	mese +="luglio"+ " ";
		    }
		    else if (codFisc.charAt(8)== 'm' || codFisc.charAt(8)== 'M') {
		    	mese +="agosto"+ " ";
		    }
		    else if (codFisc.charAt(8)== 'p' || codFisc.charAt(8)== 'P') {
		    	mese+="settembre" + " ";
		    }
		    else if (codFisc.charAt(8)== 'r' || codFisc.charAt(8)== 'R') {
		    	mese +="ottobre"+ " ";
		    }
		    else if (codFisc.charAt(8)== 's' || codFisc.charAt(8)== 'S') {
		    	mese +="novembre"+ " ";
		    }
		    else if (codFisc.charAt(8)== 't' || codFisc.charAt(8)== 'T') {
		    	mese +="dicembre" + " ";
		    }
		    else {
		    	System.out.println("Codice non valido"); //voglio che a questo punto ritorni all'inizio senza interrompersi
		    }
		    
		    
		    
			 System.out.println("\nIl tuo mese di nascita: " + mese);
			 
			 System.out.println("\nDigita 0 per ritornare al men� Codice Fiscale:");
				System.out.println("\nDigita 1 per ritornare al men� principale:");
				Scanner finaliInpuf = new Scanner(System.in);
			    int comandMenu = finaliInpuf.nextInt();

				if(comandMenu == 0) {
					CodFisc myCodFisc = new CodFisc();
					myCodFisc.setCodFiscInserito(codFisc);
					myCodFisc.StartMenu();
				} 
				else if (comandMenu == 1) {
					Main myMain = new Main();
					myMain.startMenu();
				}
	}
}
