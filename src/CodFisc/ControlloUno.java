package CodFisc;



	import java.util.Scanner;

import Main.Main;

	class ControlloUno {
		public void CheckLettera (String codFisc) {
			
//	        lettera di controllo 
			char w = '.';
			int x = codFisc.length();
			String letteraCin = codFisc.substring(15).toUpperCase();
			int sommaPari = 0;
			int sommaDisp = 0;
			
			for (int k=0; k<x; k++) {
				
//				converto tutti i caratteri in posizione pari nei numeri relativi alla tab dei pari (tranne la lettera di controllo) e li sommo tra loro
		    	if (k%2==1) {
//		    		metto tutti i caratteri pari in un array separato
		    		char carPari = codFisc.charAt(k);
		    		char [] caratteriPari = new char [16]; 
		    		int [] numPosPari = new int [16];
		    		
		    		
		    		caratteriPari[k] = carPari; 
//		    		System.out.println(caratteriPari[k]);
	 		for (int l=0; l<caratteriPari.length-1; l++) { 
		    			
		    			if (caratteriPari[l]=='a' || caratteriPari[l]=='A' ||caratteriPari[l]=='0' ) {
		    				numPosPari[l]=0;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='b' || caratteriPari[l]=='B'||caratteriPari[l]=='1') {
		    				numPosPari[l]=1;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='c' || caratteriPari[l]=='C'||caratteriPari[l]=='2') {
		    				numPosPari[l]=2;
		    				sommaPari += numPosPari[l];
		    				
		    			
		    			}
		    			else if (caratteriPari[l]=='d' || caratteriPari[l]=='D'||caratteriPari[l]=='3') {
		    				numPosPari[l]=3;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='e' || caratteriPari[l]=='E'||caratteriPari[l]=='4') {
		    				numPosPari[l]=4;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='f' || caratteriPari[l]=='F'||caratteriPari[l]=='5') {
		    				numPosPari[l]=5;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='g' || caratteriPari[l]=='G'||caratteriPari[l]=='6') {
		    				numPosPari[l]=6;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='h' || caratteriPari[l]=='H'||caratteriPari[l]=='7') {
		    				numPosPari[l]=7;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='i' || caratteriPari[l]=='I'||caratteriPari[l]=='8') {
		    				numPosPari[l]=8;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='j' || caratteriPari[l]=='J'||caratteriPari[l]=='9') {
		    				numPosPari[l]=9;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='k' || caratteriPari[l]=='K') {
		    				numPosPari[l]=10;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='l' || caratteriPari[l]=='L') {
		    				numPosPari[l]=11;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='m' || caratteriPari[l]=='M') {
		    				numPosPari[l]=12;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='n' || caratteriPari[l]=='N') {
		    				numPosPari[l]=13;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='o' || caratteriPari[l]=='O') {
		    				numPosPari[l]=14;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='p' || caratteriPari[l]=='P') {
		    				numPosPari[l]=15;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='q' || caratteriPari[l]=='Q') {
		    				numPosPari[l]=16;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='r' || caratteriPari[l]=='R') {
		    				numPosPari[l]=17;
		    				sommaPari += numPosPari[l];
		    
		    				
		    			}
		    			else if (caratteriPari[l]=='s' || caratteriPari[l]=='S') {
		    				numPosPari[l]=18;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='t' || caratteriPari[l]=='T') {
		    				numPosPari[l]=19;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='u' || caratteriPari[l]=='U') {
		    				numPosPari[l]=20;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='v' || caratteriPari[l]=='V') {
		    				numPosPari[l]=21;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='w' || caratteriPari[l]=='W') {
		    				numPosPari[l]=22;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='x' || caratteriPari[l]=='X') {
		    				numPosPari[l]=23;
		    				sommaPari += numPosPari[l];
		    				
		    			
		    			}
		    			else if (caratteriPari[l]=='y' || caratteriPari[l]=='Y') {
		    				numPosPari[l]=24;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			else if (caratteriPari[l]=='z' || caratteriPari[l]=='Z') {
		    				numPosPari[l]=25;
		    				sommaPari += numPosPari[l];
		    				
		    				
		    			}
		    			
		    		
		    	}
		    	}
		    	
//				converto tutti i caratteri in posizione dispari nei numeri relativi alla tab dei dispari e li sommo tra loro
		    	
	       	else if (k%2==0) {
		    		char carDisp = codFisc.charAt(k);
		    		char [] caratteriDisp = new char [16]; 
		    		int [] numPosDisp = new int [16];
		    		
		    		caratteriDisp[k] = carDisp; //riempio l'array
	                for (int n=0; n<caratteriDisp.length; n++) { 
		    			
		    			if (caratteriDisp[n]=='a' || caratteriDisp[n]=='A' ||caratteriDisp[n]=='0' ) {
		    				numPosDisp[n]=1;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='b' || caratteriDisp[n]=='B' ||caratteriDisp[n]=='1' ) {
		    				numPosDisp[n]=0;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='c' || caratteriDisp[n]=='C' ||caratteriDisp[n]=='2' ) {
		    				numPosDisp[n]=5;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='d' || caratteriDisp[n]=='D' ||caratteriDisp[n]=='3' ) {
		    				numPosDisp[n]=7;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='e' || caratteriDisp[n]=='E' ||caratteriDisp[n]=='4' ) {
		    				numPosDisp[n]=9;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='f' || caratteriDisp[n]=='F' ||caratteriDisp[n]=='5' ) {
		    				numPosDisp[n]=13;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='g' || caratteriDisp[n]=='G' ||caratteriDisp[n]=='6' ) {
		    				numPosDisp[n]=15;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='h' || caratteriDisp[n]=='H' ||caratteriDisp[n]=='7' ) {
		    				numPosDisp[n]=17;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='i' || caratteriDisp[n]=='I' ||caratteriDisp[n]=='8' ) {
		    				numPosDisp[n]=19;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='j' || caratteriDisp[n]=='J' ||caratteriDisp[n]=='9' ) {
		    				numPosDisp[n]=21;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='k' || caratteriDisp[n]=='K') {
		    				numPosDisp[n]=2;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='l' || caratteriDisp[n]=='L' ) {
		    				numPosDisp[n]=4;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='m' || caratteriDisp[n]=='M' ) {
		    				numPosDisp[n]=18;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='n' || caratteriDisp[n]=='N' ) {
		    				numPosDisp[n]=20;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='o' || caratteriDisp[n]=='O' ) {
		    				numPosDisp[n]=11;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='p' || caratteriDisp[n]=='P') {
		    				numPosDisp[n]=3;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='q' || caratteriDisp[n]=='Q') {
		    				numPosDisp[n]=6;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='r' || caratteriDisp[n]=='R') {
		    				numPosDisp[n]=8;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='s' || caratteriDisp[n]=='S') {
		    				numPosDisp[n]=12;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='t' || caratteriDisp[n]=='T') {
		    				numPosDisp[n]=14;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='u' || caratteriDisp[n]=='U') {
		    				numPosDisp[n]=16;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='v' || caratteriDisp[n]=='V') {
		    				numPosDisp[n]=10;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='w' || caratteriDisp[n]=='W') {
		    				numPosDisp[n]=22;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='x' || caratteriDisp[n]=='X') {
		    				numPosDisp[n]=25;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='y' || caratteriDisp[n]=='Y') {
		    				numPosDisp[n]=24;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			else if (caratteriDisp[n]=='z' || caratteriDisp[n]=='Z') {
		    				numPosDisp[n]=23;
		    				sommaDisp += numPosDisp[n];
		    				
		    				
		    			}
		    			
		    }
		    	
	}
		    	
			int tot = sommaPari + sommaDisp;
			
//			converto il resto della divisione
			if (tot%26 == 0) {
				w = 'A';
			}
			else if(tot%26 == 1) {
				w = 'B';
			}
			else if(tot%26 == 2) {
				w = 'C';
			}
			else if(tot%26 == 3) {
				w = 'D';
			}
			else if(tot%26 == 4) {
				w = 'E';
			}
			else if(tot%26 == 5) {
				w = 'F';
			}
			else if(tot%26 == 6) {
				w = 'G';
			}
			else if(tot%26 == 7) {
				w = 'H';
			}
			else if(tot%26 == 8) {
				w = 'I';
			}
			else if(tot%26 == 9) {
				w = 'J';
			}
			else if(tot%26 == 10) {
				w = 'K';
			}
			else if(tot%26 == 11) {
				w = 'L';
			}
			else if(tot%26 == 12) {
				w = 'M';
			}
			else if(tot%26 == 13) {
				w = 'N';
			}
			else if(tot%26 == 14) {
				w = 'O';
			}
			else if(tot%26 == 15) {
				w = 'P';
			}
			else if(tot%26 == 16) {
				w = 'Q';
			}
			else if(tot%26 == 17) {
				w = 'R';
			}
			else if(tot%26 == 18) {
				w = 'S';
			}
			else if(tot%26 == 19) {
				w = 'T';
			}
			else if(tot%26 == 20) {
				w = 'U';
			}
			else if(tot%26 == 21) {
				w = 'V';
			}
			else if(tot%26 == 22) {
				w = 'W';
			}
			else if(tot%26 == 23) {
				w = 'X';
			}
			else if(tot%26 == 24) {
				w = 'Y';
			}
			else if(tot%26 == 25) {
				w = 'Z';
			}
			
			
	}
		
	   String cidA = "" + w ;
	   String ok2= "" + codFisc.charAt(15);
	   
	   String cidB = ok2.toUpperCase();
	   
	    int cont = 0;
	    if (cidA.contains(cidB)) {
	    	cont += 1;
	    }
	    
		switch (cont) {   
		    case 1:
		    	break;
		    case 0:
	
			System.out.println("Lettera di controllo errata!");
     	    System.out.println("Digita 1 per ritornare al menu principale:");
			
     	   Scanner finaliInpuf = new Scanner(System.in);
     	    int comandMenu = finaliInpuf.nextInt();
            
     	    if (comandMenu == 1) {
			Main myMain = new Main();
			myMain.startMenu();
			
		}
		}

}
	}
