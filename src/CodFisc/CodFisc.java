package CodFisc;

import java.util.Scanner;

import Main.Main;
import Threads.ExampleThread;
import user.User;



public class CodFisc {
	
	
	private String codFiscInserito = "";
	
	
	public void Start() {
		
		
		
		System.out.println("\n****CALCOLA IL CODICE FISCALE INVERSO ****");
		
		System.out.println("Inserisci il codice di 16 cifre e premi invio:");
		
		Scanner codice = new Scanner(System.in);
		codFiscInserito = codice.nextLine();
		
		ControlloUno myControlloUno = new ControlloUno();
		myControlloUno.CheckLettera(codFiscInserito);
		
		codFiscInserito.replace(" ", "");
		
	
		
//		primo controllo: e' un codice di 16 cifre?
		int x = codFiscInserito.length();

		if (x == 16) {
			this.StartMenu();
		}
		else {
			 System.out.println("Il codice inserito non e' corretto!"); // anche qui voglio che a questo punto ritorni all'inizio senza interrompersi
			 this.Start();
	    }			
	}
	
	public void StartMenu() {
		
		 System.out.println("\n**** MENU ****");

			System.out.println("Scegli un'opzione:\n");
			
			System.out.println("0, chiudi App");
			System.out.println("1, per calcolare il tuo anno di nascita.");
			System.out.println("2, per calcolare il tuo mese di nascita");
			System.out.println("3, per calcolare il tuo giorno di nascita");
			System.out.println("4, per calcolare il tuo genere");
			System.out.println("5, per calcolare la tua citta' di nascita");
			System.out.println("6, per verificare se il codice inserito e' corretto");
			//TODO: creare nuova voce per stampare tutte le informzioni dell'utente
			System.out.println("7, per model Codice Fiscale");
			System.out.println("8, per model Thread");
			try ( Scanner inputA = new Scanner(System.in)) {
				
			
			int opz = inputA.nextInt();

		
			switch(opz) {
			
			case 0: 
				Main myMain = new Main();
				myMain.startMenu(); 
				break;
			case 1: 
				CalcoloData myCalcoloData = new CalcoloData();
				myCalcoloData.Calcola(codFiscInserito);
				break;
			case 2:
				CalcoloMese myCalcoloMese = new CalcoloMese();
				myCalcoloMese.Calcolo(codFiscInserito);
				break;
			case 3:
				CalcoloGiorno myCalcoloGiorno = new CalcoloGiorno();
				myCalcoloGiorno.Conteggia(codFiscInserito);
				break;
			case 4:
				CalcoloGenere myCalcoloGenere = new CalcoloGenere();
				myCalcoloGenere.Specifica(codFiscInserito);
				break;
			case 5:
				CalcoloCitta myCalcoloCitta = new CalcoloCitta();
				myCalcoloCitta.Estrai(codFiscInserito);
				break;
			case 6:
				Controllo myControllo = new Controllo();
				myControllo.ControlloLettera(codFiscInserito);
				break;
				//TODO: creare nuova caso per stampare tutte le informzioni dell'utente chiamando il metodo sotto
			case 7:
				//this.schedaPersonale();
				User utente = new User(codFiscInserito);
				System.out.println("Controllo lettera " + utente.Controllo()); 
				utente.schedaUtente();
				break;
			
			case 8:
				ExampleThread myExampleThread = new ExampleThread();
				myExampleThread.Start();
				break;
			}
			} catch (Exception e) {
				System.out.println("Non valido, riprova."); 
				
		    }
			
	    
		
		
		
	}
	
	
	
	public void setCodFiscInserito(String nuovoCod) {
		codFiscInserito = nuovoCod;
	}

	
	}



