package Threads;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Thread4 extends Thread {

	private String CodiceFiscale;
	private String DatadiNascita;
	private Date DayBorn;

	
	public void immettiCodice(String newCode) {
		this.CodiceFiscale = newCode;
	}
	

public void setCodiceFiscale(String codiceFiscale) {
	CodiceFiscale = codiceFiscale;
}

public Date getDayBorn() {
	return DayBorn;
}
public void getDatadiNascita() {
	
	String giorno = "";
	String mese = "";
	String anno = "";
	
	String b = CodiceFiscale.substring(9,11);
	int dayBorn = Integer.parseInt(b);
	
	if (dayBorn<32) {
		giorno += dayBorn;
	}
	else if (dayBorn>32 && dayBorn<71){
		giorno += dayBorn-40;
	}
	
	
	
	
	
	 if (CodiceFiscale.charAt(8)== 'a' || CodiceFiscale.charAt(8)== 'A') { 
	    	mese +="gennaio"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'b' || CodiceFiscale.charAt(8)== 'B') {
	    	mese +="febbraio" + " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'c' || CodiceFiscale.charAt(8)== 'C') {
	    	mese +="marzo" + " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'd' || CodiceFiscale.charAt(8)== 'D') {
	    	mese +="aprile" + " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'e' || CodiceFiscale.charAt(8)== 'E') {
	    	mese +="maggio"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'h' || CodiceFiscale.charAt(8)== 'H') {
	    	mese +="giugno"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'l' || CodiceFiscale.charAt(8)== 'L') {
	    	mese +="luglio"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'm' || CodiceFiscale.charAt(8)== 'M') {
	    	mese +="agosto"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'p' || CodiceFiscale.charAt(8)== 'P') {
	    	mese+="settembre" + " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 'r' || CodiceFiscale.charAt(8)== 'R') {
	    	mese +="ottobre"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 's' || CodiceFiscale.charAt(8)== 'S') {
	    	mese +="novembre"+ " ";
	    }
	    else if (CodiceFiscale.charAt(8)== 't' || CodiceFiscale.charAt(8)== 'T') {
	    	mese +="dicembre" + " ";
	    }
	    else {
	    	System.out.println("Codice non valido"); //voglio che a questo punto ritorni all'inizio senza interrompersi
	    }
	 
	 giorno += " " + mese ;
	 
	 String a = CodiceFiscale.substring(6,8);
		int yearBorn = Integer.parseInt(a);
		int yearCheck = yearBorn + 1900;
	    int yearNow = Calendar.getInstance().get(Calendar.YEAR);
	    
	 // controlla la data di nascita
	    int dateCheck = yearNow - 85;
	    
//		controlla se si hanno piu' di 85 anni e restituisce l'anno di nascita
        if (yearCheck<dateCheck) {
        	int yearYoung = yearBorn + 2000;
        	anno += yearYoung;
        }
        
        else {

        	anno += yearCheck;
        }
        
        giorno += " " + anno;
        
       
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALY);
        
            try {

            Date date = simpledateformat.parse(giorno); //born
   
            
            this.DayBorn = date;
	        
           
	      
	      

        } catch (ParseException e) {
           e.printStackTrace();
        }
        
            
            }
}
