package MathFunctions;
import java.util.Scanner;

import Main.Main;

public class ParieDispari {
	public void Start() {
		
		System.out.println("\n**** STABILISCE SE UN NUMERO � PARI O DISPARI ****");
		
		System.out.println("Inserisci un numero intero per sapere se � pari o dispari:");
		Scanner inputUno = new Scanner(System.in);
		int mioNum = inputUno.nextInt();
		
		if (mioNum%2==0) {
			System.out.println(mioNum + " " + "� pari.");
		}
		else if (mioNum%2==1) {
			System.out.println(mioNum + " " + "� dispari.");
		}
		
		System.out.println("Inserisci 1 per continuare, zero per tornare al men�."); 
	   	Scanner finaliInpuf = new Scanner(System.in);
	       int comandMenu = finaliInpuf.nextInt();

	   	
	   	if(comandMenu == 1) {
	   		Start();
	   	} else {
	   		Main myMain = new Main();
	   		myMain.startMenu();
	   	}
	}

}
