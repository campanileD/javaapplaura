package MathFunctions;


import java.util.Scanner;

import Main.Main;

public class Calcolatrice {
	
	public void Start() {
		
		    System.out.println("Inserisci un numero intero");
		    Scanner input3 = new Scanner(System.in);
			int f = input3.nextInt(); //funziona, ma non con .nextFloat()

		    System.out.println("Inserisci un numero intero");
		    Scanner input4 = new Scanner(System.in);
			int g = input4.nextInt();
			
		    System.out.println("Quale operazione vuoi svolgere?");
		    System.out.println("somma; differenza; prodotto; quoziente");
		    Scanner input5 = new Scanner(System.in);
		
		
		       if(input5.findInLine("somma") != null) {
			       System.out.println("Risultato:");
			       System.out.println(f + g);
		       }
		       else if(input5.findInLine("differenza") != null) {
			       System.out.println("Risultato:");
			       System.out.println(f - g);
		       }
		       else if(input5.findInLine("prodotto") != null) {
			       System.out.println("Risultato:");
			       System.out.println(f * g);
		       }
		       else if(input5.findInLine("quoziente") != null) {
		    	   if (g==0) {
		    		   System.out.println("Operazione non valida. Il denominatore non pu� essere zero.");
		    	   }
		    	   else {
			       System.out.println("Risultato:");
			       float h = (float) f / g; // qui voglio mettere try catch ma non funziona, ho "risolto" con l'if in riga 71
			       System.out.println(h);
		    	   }
		       }
		       
				
		       else {
			       System.out.println("Operazione non valida!"); //voglio che a questo punto ritorni alla richiesta di un'opzione senza interrompersi
			       
		       }
		       
		       System.out.println("Inserisci 1 per continuare, zero per tornare al men�."); 
		       Scanner finaliInpuf = new Scanner(System.in);
		       int comandMenu = finaliInpuf.nextInt();

		   	
		   	if(comandMenu == 1) {
		   		Start();
		   	} else {
		   		Main myMain = new Main();
		   		myMain.startMenu();
		   	}
		   	
	}

}
